package com.icdx.marketmaker.WebSockets;

import com.icdx.marketmaker.Models.Success;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public String greeting(String marketDataModel) throws Exception {
        Thread.sleep(1000); // simulated delay
        return marketDataModel;
    }
    @SendTo("/topic/alert")
    public Success alert() throws Exception {
        Success success = new Success("1","Berhasil");
        Thread.sleep(1000); // simulated delay
        return success;
    }

    @SendTo("/topic/logout")
    public Success logout() throws Exception {
        Success success = new Success("1","Gagal konek ke FIX");
        Thread.sleep(1000); // simulated delay
        return success;
    }
}
