package com.icdx.marketmaker;

import com.icdx.marketmaker.Clients.FixClientApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import quickfix.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;

@SpringBootApplication
public class MarketmakerApplication implements CommandLineRunner {
	@Autowired
	private Initiator initiator;

	public static void main(String[] args) throws SQLException {
		SpringApplication.run(MarketmakerApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {
		initiator.start();
	}

	@Bean
	public Application serverAdapter() {
		return new FixClientApplication();
	}

	@Bean
	public Initiator initiatorCon() throws FileNotFoundException, ConfigError {

		quickfix.Application application = (Application)serverAdapter();
		SessionSettings settings = new SessionSettings(new FileInputStream("config/banzai.cfg"));
		MessageStoreFactory storeFactory = new FileStoreFactory(settings);
//		LogFactory logFactory = new FileLogFactory(null);
		MessageFactory messageFactory = new DefaultMessageFactory();
		return new SocketInitiator
				(application, storeFactory, settings, messageFactory);
	}
}
