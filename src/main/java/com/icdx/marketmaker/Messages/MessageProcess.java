package com.icdx.marketmaker.Messages;

import com.icdx.marketmaker.Models.MarketMaket;
import com.icdx.marketmaker.Models.Success;
import com.icdx.marketmaker.Utils.Utils;
import org.springframework.stereotype.Component;
import quickfix.*;
import quickfix.field.*;
import quickfix.fix42.NewOrderSingle;
import quickfix.fix42.OrderCancelReplaceRequest;
import quickfix.fix42.OrderCancelRequest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.ZoneOffset;
import java.util.Date;

@Component
public class MessageProcess {

    private SessionID sessionID;
    private SessionSettings settings;

    public MessageProcess() throws FileNotFoundException, ConfigError, FieldConvertError {
        settings = new SessionSettings(new FileInputStream("config/banzai.cfg"));
        this.sessionID = new SessionID(settings.getString("BeginString"),settings.getString("SenderCompID"),
                settings.getString("TargetCompID"));
    }
    public MarketMaket OrderSingleSend(
            MarketMaket marketMaket
    ) throws SessionNotFound {
        try{
            Utils utils = new Utils();
            String j = utils.GenerateNumber();

            NewOrderSingle order = new NewOrderSingle();
            order.set(new ClOrdID(j));
            order.set(new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE));
            order.set(new Symbol(marketMaket.getMarketId().getSymbol()));
            order.set(new SecurityExchange("ICDX"));
            if(marketMaket.getMarketId().getSide().equals("1")){
                order.set(new Side(Side.BUY));
            }else if(marketMaket.getMarketId().getSide().equals("2")){
                order.set(new Side(Side.SELL));
            }
            order.set(new TransactTime(
                    new Date().toInstant()
                            .atZone(ZoneOffset.UTC)
                            .toLocalDateTime())
            );
            order.set(new OrderQty(Double.parseDouble(marketMaket.getVolume())));
            order.set(new OrdType(OrdType.LIMIT));
            order.set(new Price(Double.parseDouble(marketMaket.getPrice()) ));
            order.set(new TimeInForce(TimeInForce.DAY));
            order.set(new Account(marketMaket.getMarketId().getAccount()));
            order.setString(8000,sessionID.getSenderCompID());
            Session.sendToTarget(order,sessionID);
            marketMaket.setClOrderId(j);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return marketMaket;
    }

    public MarketMaket OrderCancelReplace(
            MarketMaket marketMaket
    ){
        Success success = new Success();
        try {

            OrderCancelReplaceRequest orderCancelReplaceRequest = new OrderCancelReplaceRequest();
            orderCancelReplaceRequest.set(new OrigClOrdID(marketMaket.getClOrderId()));
            orderCancelReplaceRequest.set(new OrderID(marketMaket.getOrderId()));
            orderCancelReplaceRequest.set(new ClOrdID(marketMaket.getClOrderId()));
            orderCancelReplaceRequest.set(new OrdType(OrdType.LIMIT));
            orderCancelReplaceRequest.set(new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE));
            orderCancelReplaceRequest.set(new SecurityExchange("ICDX"));
            orderCancelReplaceRequest.set(new Symbol(marketMaket.getMarketId().getSymbol()));
            orderCancelReplaceRequest.set(new TransactTime(
                    new Date().toInstant()
                            .atZone(ZoneOffset.UTC)
                            .toLocalDateTime())
            );
            if(marketMaket.getMarketId().getSide().equals("1")){
                orderCancelReplaceRequest.set(new Side(Side.BUY));
            }else{
                orderCancelReplaceRequest.set(new Side(Side.SELL));
            }
            orderCancelReplaceRequest.set(new Price(Float.parseFloat(marketMaket.getPrice())));
            orderCancelReplaceRequest.set(new OrderQty(Float.parseFloat(marketMaket.getVolume())));

            Session.sendToTarget(orderCancelReplaceRequest,sessionID);


        }catch (Exception ex){
            ex.printStackTrace();
        }

        return marketMaket;
    }

    public void OrderCancel(MarketMaket marketMaket){
        Success success = new Success();
        try{

            OrderCancelRequest orderCancelRequest = new OrderCancelRequest();
            orderCancelRequest.set(new OrigClOrdID(marketMaket.getClOrderId()));
            orderCancelRequest.set(new OrderID(String.valueOf(marketMaket.getOrderId())));
            orderCancelRequest.set(new ClOrdID(marketMaket.getClOrderId()));
            orderCancelRequest.set(new SecurityExchange("ICDX"));
            orderCancelRequest.set(new Symbol(marketMaket.getMarketId().getSymbol()));
            if(marketMaket.getMarketId().getSide().equals("1")){
                orderCancelRequest.set(new Side(Side.BUY));
            }else if(marketMaket.getMarketId().getSide().equals("2")){
                orderCancelRequest.set(new Side(Side.SELL));
            }

            orderCancelRequest.set(new TransactTime(
                    new Date().toInstant()
                            .atZone(ZoneOffset.UTC)
                            .toLocalDateTime())
            );
            Session.sendToTarget(orderCancelRequest,sessionID);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
