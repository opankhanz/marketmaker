package com.icdx.marketmaker.Repository;

import com.icdx.marketmaker.Models.MarketId;
import com.icdx.marketmaker.Models.MarketMaket;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MarketDataRepository extends CrudRepository<MarketMaket,MarketId>{
    @Query(nativeQuery = true)
    List<MarketMaket> GetDataAll();

    @Query(nativeQuery = true)
    MarketMaket GetData(@Param("symbol") String symbol,
                        @Param("side") String side,
                        @Param("ids") String ids,
                        @Param("account") String account);

    @Query(nativeQuery = true)
    MarketMaket GetDataByClId(@Param("clOrderId") String clOrderId,
                              @Param("account") String account);

    @Query(nativeQuery = true)
    List<MarketMaket> GetDataByAccount(@Param("account") String account);
}
