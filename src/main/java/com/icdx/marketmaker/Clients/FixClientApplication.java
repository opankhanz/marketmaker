package com.icdx.marketmaker.Clients;

import com.icdx.marketmaker.Models.MarketMaket;
import com.icdx.marketmaker.Models.Success;
import com.icdx.marketmaker.Repository.MarketDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import quickfix.*;
import quickfix.field.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@Component
public class FixClientApplication implements Application {

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private MarketDataRepository marketDataRepository;

    @Override
    public void onCreate(SessionID sessionID) {
        System.out.println("on-create :" + sessionID.toString());
    }

    @Override
    public void onLogon(SessionID sessionID) {
        System.out.println("on-logon :" + sessionID.toString());
    }

    @Override
    public void onLogout(SessionID sessionID) {
        Success success = new Success("0","Koneksi ke Server FIX gagal");
        template.convertAndSend("/topic/logout", success);
    }

    @Override
    public void toAdmin(Message message, SessionID sessionID) {
        MsgType msgType = new MsgType();
        try {
            message.getHeader().getField(msgType);
        } catch (FieldNotFound fieldNotFound) {
            fieldNotFound.printStackTrace();
        }
        if (msgType.getValue().equals(MsgType.LOGON)) {
            try {
                SessionSettings settings = new SessionSettings(new FileInputStream("config/banzai.cfg"));
                message.setField(new EncryptMethod(EncryptMethod.NONE_OTHER));
                message.setField(new Username(settings.get(sessionID).getString("Username")));
                message.setField(new Password(settings.get(sessionID).getString("Password")));
                message.setField(new ResetSeqNumFlag(true));
            } catch (ConfigError configError) {
                configError.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (FieldConvertError fieldConvertError) {
                fieldConvertError.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void fromAdmin(Message message, SessionID sessionID) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {
//        System.out.println("on-fromAdmin :" + message.toString());
    }

    @Override
    public void toApp(Message message, SessionID sessionID) throws DoNotSend {
//        System.out.println("on-toApp :" + message.toString());
    }

    @Override
    public void fromApp(Message message, SessionID sessionID) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        MsgType msgType = new MsgType();
        try {
            String hdr = message.getHeader().getField(msgType).getValue();
            if(hdr.equals("8")){
                String symbol = message.getField(new StringField(55)).getValue().toString();
                String status = message.getField(new StringField(39)).getValue().toString();
                String orderQty = message.getField(new StringField(38)).getValue().toString();
                String orderId = message.getField(new StringField(37)).getValue().toString();
                String cumQty = message.getField(new StringField(14)).getValue().toString();
                String clOrderId = message.getField(new StringField(11)).getValue().toString();
                String account = message.getField(new StringField(1)).getValue().toString();
                if(clOrderId != null){

                    if(status.equals("0")){
                        MarketMaket marketMaket = marketDataRepository.GetDataByClId(clOrderId,account);
                        marketMaket.setCumQty(cumQty);
                        marketMaket.setVolume(orderQty);
                        marketMaket.setOrderId(orderId);
                        marketDataRepository.save(marketMaket);
                    }else if(status.equals("1")){
                        MarketMaket marketMaket = marketDataRepository.GetDataByClId(clOrderId,account);
                        marketMaket.setCumQty(cumQty);
                        marketMaket.setVolume(orderQty);
                        marketMaket.setOrderId(orderId);
                        marketDataRepository.save(marketMaket);
                    }else if(status.equals("2")){
                        MarketMaket marketMaket = marketDataRepository.GetDataByClId(clOrderId,account);
                        marketDataRepository.delete(marketMaket);
                    }else if(status.equals("4")){
                        MarketMaket marketMaket = marketDataRepository.GetDataByClId(clOrderId,account);
                        marketDataRepository.delete(marketMaket);
                    }
                }

            }
        } catch (FieldNotFound fieldNotFound) {
            fieldNotFound.printStackTrace();
        }
//        System.out.println("on-fromApp :" + message.toString());
    }
}
