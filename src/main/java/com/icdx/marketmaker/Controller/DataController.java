package com.icdx.marketmaker.Controller;

import com.icdx.marketmaker.Messages.MessageProcess;
import com.icdx.marketmaker.Models.MarketId;
import com.icdx.marketmaker.Models.MarketMaket;
import com.icdx.marketmaker.Models.Success;
import com.icdx.marketmaker.Repository.MarketDataRepository;
import com.icdx.marketmaker.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import quickfix.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class DataController {
    @Autowired
    private MarketDataRepository marketDataRepository;

    @RequestMapping(value="/singleorder",method = RequestMethod.POST)
    @ResponseBody
    public Success singleorderrequest(
            @RequestParam("ids") String ids,
            @RequestParam("symbol") String symbol,
            @RequestParam("side") String side,
            @RequestParam("volume") String volume,
            @RequestParam("price") String price,
            @RequestParam("account") String account
            ) throws IOException, SQLException, SessionNotFound, DoNotSend {
            Success success  = new Success();
            try{
                MessageProcess messageProcess = new MessageProcess();
                MarketMaket marketMaket = marketDataRepository.GetData(symbol,side,ids,account);

                if(marketMaket == null){
                    MarketId marketId = new MarketId();
                    marketId.setIds(ids);
                    marketId.setSide(side);
                    marketId.setSymbol(symbol);
                    marketId.setAccount(account);
                    marketMaket = new MarketMaket();
                    marketMaket.setMarketId(marketId);
                    marketMaket.setPrice(price);
                    marketMaket.setVolume(volume);
                    marketMaket = messageProcess.OrderSingleSend(marketMaket);
                    marketDataRepository.save(marketMaket);
                }else{
                    if(marketMaket.getOrderId() != null){
                        marketMaket.setVolume(volume);
                        marketMaket.setPrice(price);
                        marketMaket = messageProcess.OrderCancelReplace(marketMaket);
                        marketDataRepository.save(marketMaket);
                    }
                }
                success.setCode("200");
                success.setMessage("success");
            }catch (Exception ex){
                success.setCode("0");
                success.setMessage("Failed");
            }
        return success;
    }

    @RequestMapping(value="/getorder",method = RequestMethod.GET)
    @ResponseBody
    public List<MarketMaket> getOrder(
    ) throws IOException, SQLException, SessionNotFound, DoNotSend {
        List<MarketMaket> marketMakets = new ArrayList<>();
        marketMakets = marketDataRepository.GetDataAll();

        return marketMakets;
    }
    @RequestMapping(value="/cancelOrder",method = RequestMethod.GET)
    @ResponseBody
    public Success Canceled(
            @RequestParam("account") String account
    ) throws SQLException, FileNotFoundException, ConfigError, FieldConvertError {
        List<MarketMaket> marketMakersList = new ArrayList<>();
        marketMakersList = marketDataRepository.GetDataByAccount(account);
        Success success = new Success();
        success.setMessage(account);
        success.setCode("200");
        for(MarketMaket marketMakers : marketMakersList){
            MessageProcess messageProcess = new MessageProcess();
            messageProcess.OrderCancel(marketMakers);
            marketDataRepository.deleteById(marketMakers.getMarketId());
        }
        return success;
    }

}
