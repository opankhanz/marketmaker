package com.icdx.marketmaker.Models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class MarketId implements Serializable {
    @Column(name = "ids")
    private String ids;
    @Column(name = "symbol")
    private String symbol;
    @Column(name = "side")
    private String side;
    @Column(name="account")
    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MarketId marketId = (MarketId) o;

        if (ids != null ? !ids.equals(marketId.ids) : marketId.ids != null) return false;
        if (symbol != null ? !symbol.equals(marketId.symbol) : marketId.symbol != null) return false;
        if (side != null ? !side.equals(marketId.side) : marketId.side != null) return false;
        return account != null ? account.equals(marketId.account) : marketId.account == null;
    }

    @Override
    public int hashCode() {
        int result = ids != null ? ids.hashCode() : 0;
        result = 31 * result + (symbol != null ? symbol.hashCode() : 0);
        result = 31 * result + (side != null ? side.hashCode() : 0);
        result = 31 * result + (account != null ? account.hashCode() : 0);
        return result;
    }
}
