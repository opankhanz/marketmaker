package com.icdx.marketmaker.Models;

import javax.persistence.*;

@Entity
@Table(name = "market_maker",schema = "dbo")
@NamedNativeQueries({
        @NamedNativeQuery(name = "MarketMaket.GetDataAll",
                query = "select * from dbo.market_maker order by symbol " ,
                resultClass = MarketMaket.class
        ),
        @NamedNativeQuery(name = "MarketMaket.GetData",
                query = "select * from dbo.market_maker where symbol = :symbol and " +
                        "ids = :ids and side = :side and account = :account "  ,
                resultClass = MarketMaket.class
        ),
        @NamedNativeQuery(name = "MarketMaket.GetDataByClId",
                query = "select * from dbo.market_maker where ClOrderId = :clOrderId and account = :account"  ,
                resultClass = MarketMaket.class
        ),
        @NamedNativeQuery(name = "MarketMaket.GetDataByAccount",
                query = "select * from dbo.market_maker where account = :account"  ,
                resultClass = MarketMaket.class
        )
})
public class MarketMaket {
    @EmbeddedId
    private MarketId marketId;

    @Column(name = "orderId")
    private String orderId;

    @Column(name = "ClOrderId")
    private String clOrderId;

    @Column(name = "volume")
    private String volume;

    @Column(name = "price")
    private String price;

    @Column(name = "cumQty")
    private String cumQty;

    public MarketId getMarketId() {
        return marketId;
    }

    public void setMarketId(MarketId marketId) {
        this.marketId = marketId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClOrderId() {
        return clOrderId;
    }

    public void setClOrderId(String clOrderId) {
        this.clOrderId = clOrderId;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCumQty() {
        return cumQty;
    }

    public void setCumQty(String cumQty) {
        this.cumQty = cumQty;
    }

    @Override
    public String toString() {
        return "MarketMaket{" +
                "marketId=" + marketId +
                ", orderId='" + orderId + '\'' +
                ", clOrderId='" + clOrderId + '\'' +
                ", volume='" + volume + '\'' +
                ", price='" + price + '\'' +
                ", cumQty='" + cumQty + '\'' +
                '}';
    }
}
